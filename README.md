fridge-backup
=============

**This project is not currently being maintained. Please get in touch if
you'd like to take it over.**

fridge-backup is a helper for creating and managing incremental backups with
BTRFS snapshots. It implements the scheme described on [Fedora Magazine](https://fedoramagazine.org/btrfs-snapshots-backup-incremental/).

Snapshots are created for the current day (systemd units are in contrib/) and
are then copied to remote BTRFS drives as specified by the user. No
encryption is added in, it's expected you'll encrypt the entire drive.

There's also no explicit restore support - just copy over the files you
want to restore using your favorite file copying program.

# Commands
```
$ sudo fridge-backup snapshot
```
Takes a snapshot of `/home` and places it at `/snapshots/home-YYYY-MM-DD`.

```
$ sudo fridge-backup sync /my/external/drive/
```
Copies the latest snapshots to your external drive. It will look to see if a
previous snapshot exists on the external drive that can be used to create
an incremental snapshot (and it will correctly use newly copied snapshots for
incremental purposes if it makes sense to).

If configured (see below), you can specify a drive name instead of a path.

```
$ fridge-backup status
```
Lists available snapshots. It is expected that this will work without being
run as root.

You can also run `fridge-backup help` and `fridge-backup help [subcommand]`
for more details on options.

# Configuration (optional)

A configuration file can be specified with `--config config.toml` (before
the subcommand) or in the default location of `/etc/fridge-backup.toml`.

An example configuration looks like:

```toml
[backups]

[backups.drive1]
path = "/run/media/user/drive1/"

[backups.drive2]
path = "/run/media/user/drive2/"
```

This allows you to run `sudo fridge-backup sync drive1` instead of needing to
specify the path to where drive1 is mounted each time. Additionally, if the
path is available, `fridge-backup status` will output information on which
snapshots are available in that backup.

## TODO
Currently paths of `/snapshots/` and `/home/` are hardcoded, this will become
configurable in the future.

At this time removal of old snapshots is a manual process. This too will be
implemented in the future.

# Dependencies
fridge-backup is a statically compiled Rust program and should work on recent
Linux distros without issue. You will need to have the `btrfs` command-line
utility installed.

The list of Rust dependencies can be seen in Cargo.toml and by running
`cargo tree`. It is kept minimal on purpose as this program will usually
be run as root.

# License
fridge-backup is (C) 2021 Kunal Mehta, under the GPL v3 or any later version.
