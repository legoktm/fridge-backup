/**
helper for creating and managing incremental backups with BTRFS snapshots
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{anyhow, Result};
use chrono::{Date, Datelike, Local, NaiveDate, TimeZone};
use clap::{App, AppSettings, Arg, SubCommand};
use indexmap::IndexMap;
use serde::Deserialize;
use std::{
    fs,
    process::{Command, Stdio},
};

const DEFAULT_CONFIG: &str = "/etc/fridge-backup.toml";

/// `config.toml` file
#[derive(Deserialize)]
struct Config {
    // Use IndexMap for consistent ordering
    backups: IndexMap<String, Backup>,
}

impl Config {
    fn new() -> Self {
        Self {
            backups: IndexMap::new(),
        }
    }
}

/// Represents a backup destination
#[derive(Deserialize, Clone)]
struct Backup {
    path: String,
}

impl Backup {
    /// Get the "local" backup
    fn local() -> Self {
        Self {
            // TODO: make this path configurable
            path: "/snapshots/".to_string(),
        }
    }

    /// If the destination is readable/available (e.g. drive is connected)
    fn is_available(&self) -> bool {
        fs::metadata(&self.path).is_ok()
    }

    /// List snapshots in the path
    fn snapshots(&self) -> Result<Vec<Snapshot>> {
        let mut snapshots: Vec<_> = fs::read_dir(&self.path)?
            .filter_map(|item| item.ok().map(|item| item.path()))
            .filter(|path| path.is_dir())
            .filter_map(|dir| {
                let date = NaiveDate::parse_from_str(
                    dir.to_str().expect("invalid file name"),
                    &format!("{}home-%Y-%m-%d", &self.path),
                );
                match date {
                    Ok(date) => Some(Snapshot::new(date.year(), date.month(), date.day())),
                    Err(err) => {
                        eprintln!("{}", err);
                        None
                    }
                }
            })
            .collect();
        snapshots.sort();
        snapshots.reverse();
        Ok(snapshots)
    }
}

/// Represents a btrfs snapshot
#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
struct Snapshot {
    year: i32,
    month: u32,
    day: u32,
}

impl Snapshot {
    fn new(year: i32, month: u32, day: u32) -> Self {
        Self { year, month, day }
    }

    /// YYYY-MM-DD
    fn name(&self) -> String {
        format!("{}-{:0>2}-{:0>2}", self.year, self.month, self.day)
    }

    /// Path on the local/source drive
    fn local_path(&self) -> String {
        format!("/snapshots/home-{}", self.name())
    }

    /// Whether the snapshot exists
    fn exists(&self) -> bool {
        fs::metadata(self.local_path()).is_ok()
    }

    fn date(&self) -> Date<Local> {
        Local.ymd(self.year, self.month, self.day)
    }

    /// How many days old the snapshot is
    fn age(&self) -> i64 {
        (Local::today() - self.date()).num_days()
    }
}

/// Take a new snapshot
fn snapshot() -> Result<()> {
    let today = Local::today();
    let snap = Snapshot::new(today.year(), today.month(), today.day());
    // First see if today's snapshot already exists
    if snap.exists() {
        println!("Snapshot {} already exists.", snap.name());
        return Ok(());
    }
    let status = Command::new("btrfs")
        .args(&["subvolume", "snapshot", "-r", "/home", &snap.local_path()])
        .stdin(Stdio::null())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .status()?;
    match status.code() {
        Some(0) => {
            println!("Snapshot {} successfully created.", snap.name());
            Ok(())
        }
        Some(code) => Err(anyhow!(
            "Snapshot {} creation failed, exited with code {}",
            snap.name(),
            code
        )),
        None => Err(anyhow!(
            "Snapshot {} creation failed, terminated by signal",
            snap.name()
        )),
    }
}

/// Print the current status
fn status(config: &Config) -> Result<()> {
    println!("Backup destinations");
    println!("===================");
    println!("local:");
    let local = Backup::local().snapshots()?;
    print_snapshots(&local);
    for (name, backup) in &config.backups {
        if !backup.is_available() {
            println!("{}: missing/disconnected", &name);
            continue;
        }
        let snapshots = backup.snapshots()?;
        println!("{}:", &name);
        print_snapshots(&snapshots);
    }
    Ok(())
}

fn print_snapshots(snapshots: &[Snapshot]) {
    for snapshot in snapshots {
        match snapshot.age() {
            0 => println!("* {} (today)", snapshot.name()),
            1 => println!("* {} (1 day old)", snapshot.name()),
            age => println!("* {} ({} days old)", snapshot.name(), age),
        }
    }
}

fn needs_copying(source: &Backup, dest: &Backup) -> Result<Vec<Snapshot>> {
    let dest_base = dest.snapshots()?[0];
    let copy = source
        .snapshots()?
        .into_iter()
        .filter(|snap| snap > &dest_base)
        .rev()
        .collect();
    Ok(copy)
}

fn identify_base(source: &Backup, dest: &Backup, snapshot: &Snapshot) -> Result<Option<Snapshot>> {
    let dest_snapshots = dest.snapshots()?;
    for snap in source.snapshots()? {
        if &snap < snapshot && dest_snapshots.contains(&snap) {
            return Ok(Some(snap));
        }
    }

    Ok(None)
}

/// Sync new local snapshots to a remote destination
fn sync(config: &Config, dest: &str) -> Result<()> {
    // Get backup for the given dest (backup name or path)
    let backup: Backup = match config.backups.get(dest) {
        Some(backup) => backup.clone(),
        None => Backup {
            path: dest.to_string(),
        },
    };
    let local = Backup::local();
    let snaps_to_copy = needs_copying(&local, &backup)?;
    for snapshot in snaps_to_copy {
        let base = identify_base(&local, &backup, &snapshot)?;
        btrfs_send_receive(&snapshot, base.as_ref(), &backup.path)?;
    }
    Ok(())
}

fn btrfs_send_receive(snap: &Snapshot, base: Option<&Snapshot>, dest: &str) -> Result<()> {
    let mut args = vec!["send".to_string()];
    if let Some(base) = base {
        args.push("-p".to_string());
        args.push(base.local_path());
        println!("Copying {} (base: {})...", snap.name(), base.name());
    } else {
        println!("Copying {} (no base)...", snap.name());
    }
    args.push(snap.local_path());

    let mut send = Command::new("btrfs")
        .args(&args)
        .stdin(Stdio::null())
        .stdout(Stdio::piped())
        .spawn()?;
    let mut receive = Command::new("btrfs")
        .args(&["receive", dest])
        // unwrap: safe because we explicitly set stdout to piped earlier
        .stdin(send.stdout.take().unwrap())
        .spawn()?;
    let send_status = send.wait()?;
    let receive_status = receive.wait()?;
    match send_status.code() {
        Some(0) => {
            // pass, need to make sure receive also exited 0
        }
        Some(code) => {
            return Err(anyhow!(
                "Snapshot {} sending failed, exited with code {}",
                snap.name(),
                code
            ))
        }
        None => {
            return Err(anyhow!(
                "Snapshot {} sending failed, terminated by signal",
                snap.name()
            ))
        }
    }
    match receive_status.code() {
        Some(0) => {
            // pass, will output success after
        }
        Some(code) => {
            return Err(anyhow!(
                "Snapshot {} receiving failed, exited with code {}",
                snap.name(),
                code
            ))
        }
        None => {
            return Err(anyhow!(
                "Snapshot {} receiving failed, terminated by signal",
                snap.name()
            ))
        }
    }
    println!("Successfully copied {}", snap.name());
    Ok(())
}

fn main() -> Result<()> {
    let matches = App::new("fridge-backup")
        .setting(AppSettings::SubcommandRequired)
        .version(env!("CARGO_PKG_VERSION"))
        .about("Backup helper for BTRFS filesystems")
        .arg(
            Arg::with_name("config")
                .long("config")
                .help("Configuration file to use")
                .takes_value(true),
        )
        .subcommand(SubCommand::with_name("snapshot").about("Take a daily snapshot"))
        .subcommand(SubCommand::with_name("status").about("List existing snapshots"))
        .subcommand(
            SubCommand::with_name("sync")
                .about("Sync snapshots to another BTRFS filesystem")
                .arg(
                    Arg::with_name("dest")
                        .help("Destination (backup name or path)")
                        .required(true),
                ),
        )
        .get_matches();
    let config: Config = match matches.value_of("config") {
        Some(path) => {
            let contents = fs::read_to_string(path)?;
            toml::from_str(&contents)?
        }
        None => {
            // No --config explicitly specified, see if default is available
            match fs::read_to_string(DEFAULT_CONFIG) {
                Ok(contents) => toml::from_str(&contents)?,
                // Couldn't read default config, fall back to defaults
                Err(_) => Config::new(),
            }
        }
    };
    match matches.subcommand() {
        ("snapshot", _) => snapshot(),
        ("status", _) => status(&config),
        // unwrap: safe because "dest" is marked as required(true)
        ("sync", Some(sub_matches)) => sync(&config, sub_matches.value_of("dest").unwrap()),
        _ => {
            unreachable!("clap requires a subcommand to be provided");
        }
    }
}
