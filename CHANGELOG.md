## 0.2.0 - 2021-04-18
* Support a configuration file for aliasing backup drive paths and names
* Fix ordering in `status` subcommand
* Introduce `sync` subcommand to replace `copy`, which now copies over all
  missing snapshots

## 0.1.0 - 2021-04-12
* Initial release
